placeTools
.service('mapService',function () {
				var markers = [];
                var doTest = function () {
                    console.log('service invoked');
                }
                var getRandomArbitrary=function(min, max) {
                    var bucket = [1,2,3,4];
                    var randomIndex = Math.floor(Math.random()*bucket.length);
                    return randomIndex;
                };

                var createVehicleMarker = function(map,location,key) {
                    var contentString = key;
                    var imageNo=getRandomArbitrary(1,4);
                    if(imageNo==1){
                        var image = 'img/corn.png';
                    }
                    else if(imageNo==2){
                        var image = 'img/fruits.png';
                    }
                    else if(imageNo==3){
                        var image = 'img/waterfilter.png';
                    }
                    else {
                        var image = 'img/medicine.png';
                    };
                    var marker = new google.maps.Marker({
                        //icon: "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=" + vehicle.vtype + "|bbT|" + vehicle.routeTag + "|" + vehicleColor + "|eee",
                        position: new google.maps.LatLng(location[0],location[1]),
                        icon: image,
                        map: map
                    });
                    marker.html = '<div>'+ key+ '</div>';

                    infoBubble = new InfoBubble({
                      map: map,
                      position: new google.maps.LatLng(location[0],location[1]),
                      maxWidth: 300,
                      disableDoubleClickZoom: true
                    });
                    var div = document.createElement('DIV');
                    div.innerHTML = 'Pale Ale $1.99 till 7pm come on in';
                     
                    infoBubble.addTab('Location',key);
                    infoBubble.addTab('Number of BeerPassers','5');
                    infoBubble.addTab('Promos',div);

                    google.maps.event.addListener(marker, 'click', function() {
                        if (infoBubble.getMap() != null) {
                            infoBubble.close();
                            infoBubble.setContent(this.html);
                            infoBubble.open(map, this);
                        }
                        console.log('marker click');
                    });

                    markers.push(marker);
                    return marker;
                } 
                  return {
                      doTest: doTest,
                      getRandomArbitrary: getRandomArbitrary,
                      createVehicleMarker: createVehicleMarker
                  };
    })

  .directive('ionGooglePlaceMap', ['$rootScope','mapService','dataFactory','$q','$timeout', function($rootScope,mapService,dataFactory,$q,$timeout) {
  	return {
  		restrict: 'E',
  		template: [
  			'<div class="ion-place-tools-map-holder" ng-class="{ \'visible\': visible }">',
  				'<div class="ion-place-tools-map-wrapper">',
	  				'<div class="ion-place-tools-map"></div>',
	  				'<div class="autocomplete-wrap">',
	  					'<ion-google-place radius="options.radius" placeholder="Your address" location-changed="locationChanged" ng-model="data.address" required name="address"/>',
	  				'</div>',
	  				'<div class="controls">',
	  					'<button type="button" class="button button-stable" ng-click="hideModal()">Cancel</button>',
	  					'&nbsp;',
	  					'<button type="button" class="button button-positive" ng-click="callSuccess()">OK</button>',
	  				'</div>',
	  			'</div>',
  			'</div>'
  		].join(''),
  		scope: {
        visible: '=',
        data: '=',
        options: '=',
        onSuccess: '&'
      },
  		replace: true,
  		link: function (scope, element, attrs) {
  			var markers = [];
  			options = angular.extend({
  				radius: 15000,
  				//fitBounds: true,
  				marker: true
  			}, scope.options);
  			var zValue = 10;
          	var lat = 51.508742;
          	var lng = -0.120850;
          	var vehiclesInQuery = {};
            var markerClusterer = null;
            var circle;
            

  			var mapEl = element.find('div')[0].children[0],
  			
  				mapProp = {
				    center: new google.maps.LatLng(lat,lng),
				    zoom: zValue,
				    mapTypeId: google.maps.MapTypeId.ROADMAP
				  },
			  	map = new google.maps.Map(mapEl, mapProp),
			  	geocoder = new google.maps.Geocoder(),
			  	marker = new google.maps.Marker({
			  		position: mapProp.center,
			  		map: map
			  		//draggable: true
			  	}),
			  	circle = new google.maps.Circle({
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: '#FF0000',
                    fillOpacity: 0.35,
                    map: map,
                    center: new google.maps.LatLng(51.508742,-0.120850),
                    radius: 5000,
                    editable: true
                  });


			  if (!options.marker) {
			  	marker.setVisible(false);
			  }

			  scope.hideModal = function () {
			  	scope.visible = false;
			  };

			  scope.callSuccess = function () {
			  	scope.hideModal();
			  	if (scope.onSuccess) {
            scope.onSuccess()();
          }
			  };

			  scope.locationChanged = function (address) {
			  	console.log('locationChanged fired');
					geocoder.geocode({
					  address: address
					}, function(results, status) {
					  if (status == google.maps.GeocoderStatus.OK) {
					  	scope.data.latitude = results[0].geometry.location.lat();
			  			scope.data.longitude = results[0].geometry.location.lng();
					    map.setCenter(results[0].geometry.location);
              console.log(results[0].geometry.location);
					    marker.setPosition(results[0].geometry.location);
					    if (options.fitBounds) {
					    	map.fitBounds(results[0].geometry.viewport);
					    }
					    circle.setCenter(results[0].geometry.location);
					    geoQuery.updateCriteria({
		                    //center: locations["Folsom"],
		                    center: [scope.data.latitude, scope.data.longitude],
		                    radius: 5
		                });
					  }
					});
			  };

        $rootScope.$on('CurrentPosition', function(event, data) {
            console.log('event fired map got CurrentPosition'+data.position.coords.latitude+":"+data.position.coords.longitude);
              scope.data.latitude = data.position.coords.latitude;
              scope.data.longitude = data.position.coords.longitude;
              var myLatlng = {lat: scope.data.latitude, lng: scope.data.longitude};
              map.setCenter(myLatlng);
              console.log(myLatlng);
              marker.setPosition(myLatlng);
              circle.setCenter(myLatlng);
              geoQuery.updateCriteria({
                        //center: locations["Folsom"],
                        center: [scope.data.latitude, scope.data.longitude],
                        radius: 5
                    });

          });

			  var geoCodeByCoords = function (latLng) {
			  	geocoder.geocode({
			  		latLng: latLng
			  	}, function (results, status) {
			  		if (status == google.maps.GeocoderStatus.OK) {
			  			scope.data.address = results[0].formatted_address;
			  			scope.data.latitude = results[0].geometry.location.lat();
			  			scope.data.longitude = results[0].geometry.location.lng();
			  			scope.$apply();
			  			map.setCenter(results[0].geometry.location);
			  			marker.setPosition(results[0].geometry.location);
			  			if (options.fitBounds) {
					    	map.fitBounds(results[0].geometry.viewport);
					    }
			  		}
			  	});
			  };

			  		getRandomArbitrary=function(min, max) {
	                    var bucket = [1,2,3,4];
	                    var randomIndex = Math.floor(Math.random()*bucket.length);
	                    return randomIndex;
	                };

			        createVehicleMarker = function(location,key) {
                    var contentString = key;
                    var imageNo=getRandomArbitrary(1,4);
                    if(imageNo==1){
                        var image = 'img/corn.png';
                    }
                    else if(imageNo==2){
                        var image = 'img/fruits.png';
                    }
                    else if(imageNo==3){
                        var image = 'img/waterfilter.png';
                    }
                    else {
                        var image = 'img/medicine.png';
                    };
                    var marker = new google.maps.Marker({
                        //icon: "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=" + vehicle.vtype + "|bbT|" + vehicle.routeTag + "|" + vehicleColor + "|eee",
                        position: new google.maps.LatLng(location[0],location[1]),
                        icon: image,
                        map: map
                    });
                    marker.html = '<div>'+ key+ '</div>';

                    infoBubble = new InfoBubble({
                      map: map,
                      position: new google.maps.LatLng(location[0],location[1]),
                      maxWidth: 300,
                      disableDoubleClickZoom: true
                    });
                    //var div = document.createElement('DIV');
                    //div.innerHTML = 'Pale Ale $1.99 till 7pm come on in';
                     
                    //infoBubble.addTab('Location',key);
                    //infoBubble.addTab('Number of BeerPassers','5');
                    //infoBubble.addTab('Promos',div);

                    google.maps.event.addListener(marker, 'click', function() {
                        if (infoBubble.getMap() != null) {
                            infoBubble.close();
                            infoBubble.setContent(this.html);
                            infoBubble.open(map, this);
                        }
                        console.log('marker click');
                    });

                    markers.push(marker);
                    return marker;
                }; 

			  /*google.maps.event.addListener(map, 'dragend', function() {
			  	geoCodeByCoords(map.getCenter());
			  });*/

			  var infoWindow = null;

			  google.maps.event.addListener(map, 'click', function (ev) {
			  	geocoder.geocode({
			  		latLng: ev.latLng
			  	}, function (results, status) {
			  		if (status == google.maps.GeocoderStatus.OK) {
			  			if (infoWindow) {
			  				infoWindow.close();
			  			}
				  		infoWindow = new google.maps.InfoWindow({
		            content: results[0].formatted_address,
		            position: ev.latLng
		          });
		          infoWindow.open(map);
				  	}
			  	});
			  });

			  /*google.maps.event.addListener(marker, 'dragend', function () {
      				geoCodeByCoords(marker.getPosition());
      			});*/

			  /*navigator.geolocation.getCurrentPosition(function (position) {
          			latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
          			geoCodeByCoords(latLng);
        	});*/

		        scope.$watch('options.marker', function(val) {
					  	options.marker = val;
					  	marker.setVisible(options.marker);
					  });

		        var geoQuery = dataFactory.getGeoFireNode().$query({
                    //center: locations["Folsom"],
                    center: [lat, lng],
                    radius: 2
                });

                google.maps.event.addListener(circle, 'radius_changed', function() {
                  console.log(circle.getRadius());
                  //console.log('bounds changed called');
                  var newLat = circle.getCenter().lat();
                  var newLong = circle.getCenter().lng();
                  //console.log("Lat"+ newLat+"Lon"+newLong);
                  var newRadius = circle.getRadius();
                  var radiusNew = (newRadius / 1000).toFixed(2);
                  console.log('radius changed: '+newLat + ' '+newLong+ " "+ newRadius);
                  geoQuery.updateCriteria({
                      center: [newLat, newLong],
                      radius: newRadius / 1000
                  });
                });

                google.maps.event.addListener(circle, 'bounds_changed', function() {
                  //console.log(circle.getRadius());
                  //console.log('bounds changed called');
                  var newLat = circle.getCenter().lat();
                  var newLong = circle.getCenter().lng();
                  var newRadius = circle.getRadius();
                  var radiusNew = (newRadius / 1000).toFixed(2);
                  console.log('bounds changed: '+newLat + ' '+newLong+ " "+ newRadius);
                  geoQuery.updateCriteria({
                      center: [newLat, newLong],
                      radius: newRadius / 1000
                  });
                });

                scope.boundsChanged = function (evt, evtMap) {
                  console.log('bounds changed called');
                  var newLat = this.getCenter().lat();
                  var newLong = this.getCenter().lng();
                  var newRadius = this.getRadius();
                  $scope.radiusNew = (newRadius / 1000).toFixed(2);
                  //console.log('bounds changed: '+newLat + ' '+newLong+ " "+ newRadius);
                  geoQuery.updateCriteria({
                      center: [newLat, newLong],
                      radius: newRadius / 1000
                  });
              }

            var onKeyEnteredRegistration = geoQuery.on("key_entered","KEY:ENTERED");
            var onKeyExitedRegistration = geoQuery.on("key_exited","KEY:EXIT");
            var onReadyRegistration = geoQuery.on("ready", "KEY:READY");

            scope.$on("KEY:ENTERED", function (event, key, location, distance) {
                console.log(key);
                var breweryData=dataFactory.getFirebaseRoot().child("list").orderByChild('Number').equalTo(key);
                breweryData.on("value", function(snapshot) {
                    var brewery=snapshot.val();
                    console.log(JSON.stringify(brewery));
                    var markerText=JSON.stringify(brewery);
                    console.log(markerText);
                    //console.log(brewery.locations[0].streetAddress);
                  vehiclesInQuery[key] =createVehicleMarker(location, markerText);
                }, function (errorObject) {
                  console.log("The read failed: " + errorObject.code);
                });
            }); 

            scope.$on("KEY:EXIT", function (event, key, location, distance) {
                //console.log('EXIT');
                if(vehiclesInQuery[key]) {
                    vehiclesInQuery[key].setMap(null);
                    console.log('nulled out '+key);
                }
                console.log(key+' :'+ location[0]+ ':'+ location[1]+" exiting");
            });



  		}
  	};
  }])


.factory('dataFactory', ['mapService','$geofire','$firebase','$q', function(mapService,$geofire,$firebase,$q){
    var BREWERY_REF= "https://notaryca.firebaseio.com/";
    var GEOFIRE_REF = "https://geonotary.firebaseio.com/";
    var firebaseRef= new Firebase(BREWERY_REF);
    var geoFire = new $geofire(new Firebase(GEOFIRE_REF));
    var getFirebaseRoot = function(){
        return firebaseRef;
    };
    var getGeoFireNode = function(){
        return geoFire;   
    }
    var getFoodTruckNode = function(){
        return getFirebaseRoot().child("FoodTrucks");   
    }
    var addData = function(data, locationData){
        // persist our data to firebase
        var ref = getFoodTruckNode();
        return  $firebase(ref).$push(data).then(function(childRef){
               addGeofireData({key: childRef.name(), latitude: locationData.latitude, longitude: locationData.longitude});
        });
    };
    var addGeofireData = function(data){
        var defer = $q.defer();  
        geoFire.set(data.key, [data.latitude, data.longitude]).then(function() {
            defer.resolve();
          }).catch(function(error) {
            defer.reject(error);
        });
        return defer.promise;
    };
    var getData = function(callback){
        var ref = getFoodTruckNode();
        return $firebase(ref).$asArray();
    }
    var service = {
        addData : addData,
        getData: getData,
        getFirebaseRoot: getFirebaseRoot,
        getGeoFireNode : getGeoFireNode
    };
    return service;
}])

