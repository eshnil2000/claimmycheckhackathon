(function () {
    angular.module('beerPass').controller('DocumentController', ['$timeout','$q','$scope', '$ionicModal', 'InvoiceService', DocumentController]);

    function DocumentController($timeout,$q,$scope, $ionicModal, InvoiceService) {
        var vm = this;

        setDefaultsForPdfViewer($scope);

        // Initialize the modal view.
        $ionicModal.fromTemplateUrl('pdf-viewer.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            vm.modal = modal;
        });

         vm.writeToFile=function(fileName, data) {
                data = JSON.stringify(data, null, '\t');
                window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (directoryEntry) {
                    directoryEntry.getFile(fileName, { create: true }, function (fileEntry) {
                        fileEntry.createWriter(function (fileWriter) {
                            fileWriter.onwriteend = function (e) {
                                // for real-world usage, you might consider passing a success callback
                                console.log('Write of file "' + fileName + '"" completed.');
                            };

                            fileWriter.onerror = function (e) {
                                // you could hook this up with our global error handler, or pass in an error callback
                                console.log('Write failed: ' + e.toString());
                            };

                            var blob = new Blob([data], { type: 'text/plain' });
                            fileWriter.write(blob);
                        }, errorHandler.bind(null, fileName));
                    }, errorHandler.bind(null, fileName));
                }, errorHandler.bind(null, fileName));
            }

      

        vm.createInvoice = function () {
            var invoice = getDummyData();

            InvoiceService.createPdf(invoice)
                .then(function (pdf) {
                    
                    var blob = new Blob([pdf], { type: 'application/pdf' });
                    
                    $scope.pdfUrl = URL.createObjectURL(blob);
                    
                    var blob1 = new Blob(["Hello, world!"], {type: "text/plain;charset=utf-8"});
                    //HTML saveAs uses <script src="js/FileSaver.js"></script>
                    saveAs(blob, "invoice.pdf");
           
                    vm.modal.show();
                });
        };

        // Clean up the modal view.
        $scope.$on('$destroy', function () {
            vm.modal.remove();
        });

        return vm;
    }

    function setDefaultsForPdfViewer($scope) {
        $scope.scroll = 0;
        $scope.loading = 'loading';

        $scope.onError = function (error) {
            console.error(error);
        };

        $scope.onLoad = function () {
            $scope.loading = '';
        };

        $scope.onProgress = function (progress) {
            console.log(progress);
        };
    }

    function getDummyData() {
        return {
            Date: new Date().toLocaleDateString("en-IE", { year: "numeric", month: "long", day: "numeric" }),
            AddressFrom: {
                Name: chance.name(),
                Address: chance.address(),
                Country: chance.country({ full: true })
            },
            AddressTo: {
                Name: chance.name(),
                Address: chance.address(),
                Country: chance.country({ full: true })
            },
            Items: [
                { Description: 'iPhone 6S', Quantity: '1', Price: '€700' },
                { Description: 'Samsung Galaxy S6', Quantity: '2', Price: '€655' }
            ],
            Subtotal: '€2010',
            Shipping: '€6',
            Total: '€2016'
        };
    }
})();