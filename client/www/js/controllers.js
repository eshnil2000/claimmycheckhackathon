/*global TransitionType, UUIDjs*/

angular.module('beerPass') 
    //directive to provide rating capability
    .directive('starRating',
    function() {
        return {
            restrict : 'A',
            template : '<ul class="rating">'
            + ' <li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">'
            + '\u2605'
            + '</li>'
            + '</ul>',
            scope : {
                ratingValue : '=',
                max : '=',
                onRatingSelected : '&'
            },
            link : function(scope, elem, attrs) {
                var updateStars = function() {
                    scope.stars = [];
                    for ( var i = 0; i < scope.max; i++) {
                        scope.stars.push({
                            filled : i < scope.ratingValue
                        });
                    }
                };

                scope.toggle = function(index) {
                    scope.ratingValue = index + 1;
                    scope.onRatingSelected({
                        rating : index + 1
                    });
                };

                scope.$watch('ratingValue',
                    function(oldVal, newVal) {
                        if (newVal) {
                            updateStars();
                        }
                    }
                );
            }
        };
    }
)
.controller('MenuCtrl', function($scope,$state){

        $scope.showIntro= function(form) {
            $state.go('app.intro');
        };

         })
.controller('introCtrl',['$state','$ionicSlideBoxDelegate', '$document','$scope',function($state,$ionicSlideBoxDelegate, $document,$scope) {
            $scope.slideChanged = function(index) {
            var slides = $ionicSlideBoxDelegate.slidesCount();
            var increment = $document[0].getElementsByClassName('increment');
            increment[0].style.width = (1+19*index/(slides-1))*5+'%';
          };

            $scope.rating = 5;
            console.log('introctrl');
            $scope.rateFunction = function(rating) {
                //alert('Thanks for giving us a' + rating+ ' star rating!');
            };

            $scope.skipIntro=function(){
                console.log('in skipIntro');
                 $state.go('app.locations');
            };
     

}])

.controller('MyCtrl', function($rootScope,$ionicLoading,mapService,$scope, $timeout,$cordovaGeolocation) {
        //mapService.doTest();
                $scope.locate = function(){

        $cordovaGeolocation
          .getCurrentPosition()
          .then(function (position) {
            $rootScope.$broadcast('CurrentPosition',{position:position});
            $ionicLoading.hide();
            //alert(position.coords.latitude+":"+position.coords.longitude);
            $scope.showMap();

          }, function(err) {
            // error
            console.log("Location error!");
            console.log(err);
          });

      };

        $ionicLoading.show({
                template: 'Obtaining current location...'
            });
        $scope.locate();
        var imageNo=mapService.getRandomArbitrary(1,4);
        //console.log(imageNo);
        
        $scope.x = { visible: false };
        $scope.data = { //will contain address and latitude/longitude
          address: ''
        };
        $scope.options = {
          radius: 2000, // radius for place search
          fitBounds: true, // fit map to bounds returned by search api
          marker: false // show draggable marker on map
        };

        $scope.showMap = function () {
          $scope.x.visible = true;
        };

        // this will be called on "OK" button click
        $scope.successCallback = function () {
          alert($scope.data.address + ' selected!');
        };

        $scope.showAlert = function (msg) {
          alert(msg);
        };

        setTimeout(function () {
          $scope.options.marker = true;
          $scope.$apply();
        }, 3000);


      })

.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}])
.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl){
        var fd = new FormData();
        fd.append('file', file);
        console.dir(file);
        console.log('success appending file');
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(){
        })
        .error(function(){
        });
    }
}])

.controller('fileCtrl', ['$scope', 'fileUpload', function($scope, fileUpload){
            $scope.uploadFile = function(){
               var file = $scope.myFile;
               
               console.log('file is ' );
               console.dir(file);
               
               var uploadUrl = "/fileUpload";
               fileUpload.uploadFileToUrl(file, uploadUrl);
            };
         }])

.controller('ClaimCtrl', ['$location','fileUpload','$rootScope','$timeout', '$ionicModal', 'InvoiceService','$http', '$ionicLoading','$firebase','$rootScope', '$state','$timeout','$geofire','GEOFIRE_REF','$q','$scope',
        function($location,fileUpload,$rootScope,$timeout, $ionicModal, InvoiceService,$http,$ionicLoading,$firebase,$rootScope, $state,$timeout,$geofire,GEOFIRE_REF,$q,$scope){
            $scope.ClaimName=$rootScope.vars.ClaimName;
            $scope.amount=$rootScope.vars.amount;
            $scope.Check_No=$rootScope.vars.Check_No;
            $scope.date=$rootScope.vars.date;

            $scope.address='';
            $scope.telephone='';
            $scope.form1='';
            $scope.notary='';
            $scope.notaryList=[];
            //$scope.amount=null;
            $scope.beerArray=[];
            $scope.leftButtons = [];
            var notary='https://notaryca.firebaseio.com/';
            var notaryRef=new Firebase(notary);
            var notaryPath=notaryRef.child('list');

            $scope.sendMail=function(){
                var email = 'getmadollah@saccounty.net';
                var subject = 'Unclaimed Check No: '+$scope.Check_No;
                var body= encodeURIComponent('Name: '+ $scope.ClaimName + '\r\n'+ 'Dated: '+$scope.date + '\r\n'+ 'Amount: '+ $scope.amount);
                var mailto_link = 'mailto:' + email + '?subject=' + subject+'&body='+body;

                //$location.path(mailto_link);
                win = window.open(mailto_link, 'emailWindow');
                //if (win && win.open && !win.closed) win.close();

            }
            $scope.$on('$destroy', function () {
                $scope.modal.remove();
            });

             $scope.Next = function (infodata) {
                $scope.notaryList=[];
              $scope.value=infodata;      
              console.log($scope.value);
              var test = notaryPath.orderByChild('Zip_Code').startAt($scope.value.FirstName).endAt($scope.value.FirstName).limitToFirst(100).
                       once('value', function(snap) {
                        for(key in snap.val()){  
                            console.log(snap.val()[key]);
                            $scope.notaryList.push(snap.val()[key]);
                            $scope.$apply();
                        };
                    });
            };
            $scope.findNotary=function(infodata){
                $scope.notary=infodata;
                console.log('zip is'+$scope.notary);
                var test = notaryPath.orderByChild('Zip_Code').startAt($scope.notary.zipCode).endAt($scope.notary.zipCode).limitToFirst(5).
                       once('value', function(snap) {
                        for(key in snap.val()){  
                            console.log(key);
                        };
                    });
            };


            $scope.editPDF=function(){
                    $scope.modal.hide(); 
                    //$scope.modalEdit.show();
                    $scope.addDialog.show();

            };
            $scope.doneEdit=function(form1){
                $scope.form1=form1;
                //console.log(form1.choreDesc.$modelValue);
                console.log(form1.ClaimName.$modelValue);
                $scope.ClaimName=form1.ClaimName.$modelValue;
                $scope.amount=form1.amount.$modelValue;
                $scope.Check_No=form1.Check_No.$modelValue;
                $scope.date=form1.date.$modelValue;
                if(form1.Address.$modelValue){
                    $scope.address=form1.Address.$modelValue;
                }
                else{
                    $scope.address='';
                }
                if(form1.Telephone.$modelValue){
                    $scope.telephone=form1.Telephone.$modelValue;
                }
                else{
                    $scope.telephone='';
                }

                console.log('doneEditClaim');
                //$scope.modalEdit.hide(); 
                $scope.addDialog.hide();
                $scope.createInvoice();               
            };

            $scope.createPDF=function(){
                console.log('in create claim');
                var invoice = getDummyData();

                InvoiceService.createPdf(invoice)
                    .then(function (pdf) {
                        
                        var blob = new Blob([pdf], { type: 'application/pdf' });
                        
                        $scope.pdfUrl = URL.createObjectURL(blob);
                        
                        var blob1 = new Blob(["Hello, world!"], {type: "text/plain;charset=utf-8"});
                        //HTML saveAs uses <script src="js/FileSaver.js"></script>
                        saveAs(blob, "invoice.pdf");
                        //vm.writeToFile('test.pdf', { foo: 'bar' });
                        //window.URL.revokeObjectURL($scope.pdfUrl);
                        //vm.test();
                        //saveFile(blob);
                        // Display the modal view
                       // $scope.modal.show();
                    });

            }

            function setDefaultsForPdfViewer($scope) {
                $scope.scroll = 0;
                $scope.loading = 'loading';

                $scope.onError = function (error) {
                    console.error(error);
                };

                $scope.onLoad = function () {
                    $scope.loading = '';
                };

                $scope.onProgress = function (progress) {
                    console.log(progress);
                };
            };

            setDefaultsForPdfViewer($scope);

             $ionicModal.fromTemplateUrl('views/addChore.html', function(modal) {
                    $scope.addDialog = modal;
                }, {
                    scope: $scope,
                    animation: 'slide-in-up'
                });

            // Initialize the modal view.
            $ionicModal.fromTemplateUrl('pdf-viewer.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modal = modal;
            });

      
            // Initialize the modal view.
            $ionicModal.fromTemplateUrl('pdf-editor.html', {
                scope: $scope,
                controller: 'DemoCtrl',
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modalEdit = modal;
            });

            $scope.writeToFile=function(fileName, data) {
                data = JSON.stringify(data, null, '\t');
                window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (directoryEntry) {
                    directoryEntry.getFile(fileName, { create: true }, function (fileEntry) {
                        fileEntry.createWriter(function (fileWriter) {
                            fileWriter.onwriteend = function (e) {
                                // for real-world usage, you might consider passing a success callback
                                console.log('Write of file "' + fileName + '"" completed.');
                            };

                            fileWriter.onerror = function (e) {
                                // you could hook this up with our global error handler, or pass in an error callback
                                console.log('Write failed: ' + e.toString());
                            };

                            var blob = new Blob([data], { type: 'text/plain' });
                            fileWriter.write(blob);
                        }, errorHandler.bind(null, fileName));
                    }, errorHandler.bind(null, fileName));
                }, errorHandler.bind(null, fileName));
            };


            function getDummyData(claimType) {
                console.log('getDummy'+ $rootScope.claimOrDonate);
                return {
                    Date: new Date().toLocaleDateString("en-IE", { year: "numeric", month: "long", day: "numeric" }),
                    
                    ClaimInfo:{
                        Name: $scope.ClaimName,
                        Amount: $scope.amount,
                        CheckNo: $scope.Check_No,
                        Dated: new Date($scope.date).toLocaleDateString("en-IE", { year: "numeric", month: "long", day: "numeric" }),
                        Address:$scope.address,
                        Telephone:$scope.telephone,
                        claimType: claimType,
                        claimOrDonate:$rootScope.claimOrDonate
                    }
                };
            };

            $scope.createInvoice = function (claimType) {
                var invoice = getDummyData(claimType);

                InvoiceService.createPdf(invoice)
                    .then(function (pdf) {
                        
                        var blob = new Blob([pdf], { type: 'application/pdf' });
                        
                        $scope.pdfUrl = URL.createObjectURL(blob);
                        
                        var blob1 = new Blob(["Hello, world!"], {type: "text/plain;charset=utf-8"});
                
                        $scope.modal.show();
                    });
            };

}])

.controller('DemoCtrl', ['$rootScope','$timeout', '$ionicModal', 'InvoiceService','$http', '$ionicLoading','$firebase','$rootScope', '$state','$timeout','$geofire','GEOFIRE_REF','$q','$scope',
        function($rootScope,$timeout, $ionicModal, InvoiceService,$http,$ionicLoading,$firebase,$rootScope, $state,$timeout,$geofire,GEOFIRE_REF,$q,$scope){
            //$rootScope.vars=null;
            $scope.ClaimName='';
            $scope.amount='';
            $scope.Check_No='';
            $scope.date='';

            $scope.address='';
            $scope.telephone='';
            $scope.form1='';
            $scope.notary='';
            $scope.notaryList=[];
            $scope.amount=null;
            $scope.beerArray=[];
            $scope.leftButtons = [];
            var addButton = {};
            addButton.type = "button-clear";
            addButton.content = '<i class="icon ion-ios7-plus-outline"></i>';
            addButton.tap = function(e) {
                $scope.showAddChangeDialog('add');
            }
            $scope.leftButtons.push(addButton);
            //console.log(addButton.type);
            // Define item buttons
            $scope.itemButtons = [{
                text: 'Delete',
                type: 'button-assertive',
                onTap: function(item) {
                    $scope.removeItem(item);
                }
            }, {
                text: 'Edit',
                type: 'button-calm',
                onTap: function(item) {
                    $scope.showEditItem(item);
                }
            }];

            $rootScope.$on('found',function(event,result){
                console.log('found',result.foundName.foundName);
                 addthis_share = 
                { 
                    templates: {
                    twitter: 'found '+' $'+result.foundAmount.foundAmount+' for my buddy '+result.foundName.foundName +' on '+ 'http://sacgetucheckback.org',
                    }
                }
            });

            $scope.byCheckNo=false;
            $scope.toggleChange = function() {
                if ($scope.byCheckNo == false) {
                    $scope.byCheckNo = true;
                } else
                    $scope.byCheckNo = false;
                console.log('testToggle changed to ' + $scope.byCheckNo);
            };

            $scope.removeItem = function(item) {

                var deferred=$q.defer();
                var promise=deferred.promise;

                promise.then(function(result) {
                    //console.log($scope.chores[item.choreID]);
                    //$scope.choreList.splice($scope.choreList.indexOf(item), 1);
                    $scope.beerArray.splice($scope.beerArray.indexOf(item), 1);
                }, function(reason){
                    alert('Error'+reason);

                });
                //deferred.resolve(SharedData.get().child("chores").child(userSession.user.uid).child("chore").child(item.choreID).remove());
                deferred.resolve(1);
            }
            var beerPass='https://unclaimed3.firebaseio.com/';
            
            var beerRef=new Firebase(beerPass);
            
            var breweryPath=beerRef.child('saccounty');
            

            $scope.clearSearch=function(){
                console.log('clearing search');
            }

            $scope.goClaims=function(typeDonateClaim){
                console.log('going to claims');
                $rootScope.vars=$rootScope.dataObj;
                console.log($rootScope.vars);
                $rootScope.claimOrDonate=typeDonateClaim;
                console.log($rootScope.claimOrDonate);
                //$scope.vars=getDummyData();
                //var objtest={'var':'1','var2':'2'};
                $state.go('app.claims');

            }

           

            $scope.capitalizeFirstLetter=function(string) {
                return string.charAt(0).toUpperCase() + string.slice(1);
            }

            $scope.asyncSearch = function (str) {
                //console.log(str);
                $scope.notaryList=[];
                var array = [];
                str=$scope.capitalizeFirstLetter(str);
                console.log(str);
                if(str.length>0){
                    //console.log('string is: '+ str);
                    //var test = breweryPath.orderByKey().startAt(str).limitToFirst(50).
                    if($scope.byCheckNo){
                        var test = breweryPath.orderByChild('Check_Number').startAt(str).limitToFirst(50);
                    }
                    else
                    {
                       var test = breweryPath.orderByChild('Payee_Name').startAt(str).limitToFirst(10);
                    
                    }
                    // //var test = breweryPath.orderByKey().limitToFirst(20).
                      test.once('value', function(snap) {
                        for(key in snap.val()){  
                            console.log('key is:'+key);
                            //console.log(snap.val()[key]['Payee_Name']);
                            //array.push({'name':snap.val()[key]});
                            if($scope.byCheckNo){
                                var valueObj={"key":key,"payeeName":snap.val()[key]['Payee_Name'],"name":snap.val()[key]['Check_Number'],"Amount":snap.val()[key]['Check_Amount'], "Date":snap.val()[key]['Check_Date'],"Check_No":snap.val()[key]['Check_Number']}
                                //console.log(valueObj);
                                var value="Name: "+snap.val()[key]['Check_Number']+" Amount:"+snap.val()[key]['Check_Amount']+" Date:"+snap.val()[key]['Check_Date']+" Check_No:"+snap.val()[key]['Check_Number'];
                            }
                            else
                            {
                                var valueObj={"key":key,"payeeName":snap.val()[key]['Payee_Name'],"name":snap.val()[key]['Payee_Name'],"Amount":snap.val()[key]['Check_Amount'], "Date":snap.val()[key]['Check_Date'],"Check_No":snap.val()[key]['Check_Number']}
                                //console.log(valueObj);
                                var value="Name: "+snap.val()[key]['Payee_Name']+" Amount:"+snap.val()[key]['Check_Amount']+" Date:"+snap.val()[key]['Check_Date']+" Check_No:"+snap.val()[key]['Check_Number'];
                            }    
                                //array.push({'name':value});
                            array.push(valueObj);
                            //console.log(array);
                            //console.log(snap.val()[key]);
                        };
                        //console.log("array length is now"+array.length);
                        //$ionicLoading.hide();
                        $scope.autocompleteInputAsync.searchlist = array;
                        $scope.$root.$broadcast($scope.autocompleteInputAsync.ID); 
                    })
                      console.log(test);
                }
              }

            $scope.setModel = function (item) {
                $scope.selectedItem = item;
                $scope.beerArray.push(item.name);
                //$scope.autocompleteInputAsync='';    
            };

            $scope.setModelAsync = function (item) {
                $scope.selectedItemAsync = item;
                $scope.beerArray.push(item);
                var foundName=item.payeeName;
                
                $scope.key=item.key;
                $scope.ClaimName=item.payeeName;
                $scope.amount=item.Amount.replace(/[^a-zA-Z0-9.]/g, "");
                var foundAmount=$scope.amount;
                $scope.Check_No=item.Check_No;
                $scope.date=item.Date;
                $rootScope.dataObj=
                    {'ClaimName':$scope.ClaimName,
                      'amount': $scope.amount,
                      'Check_No':$scope.Check_No,
                      'date':$scope.date,
                      'key':$scope.key
                };
                console.log($rootScope.dataObj);
                $rootScope.$broadcast('found',{foundName:{foundName},foundAmount:{foundAmount}});
                //console.log(item.name);
                //$scope.autocompleteInputAsync='';
            };

            $scope.autocompleteInput = {
                'propNameToDisplay': 'name',
                'placeholder': 'Search Country - Static',
                'ID':'StaicData',
                'listClass': ['list-border-energized'],
                'labelContainerClass': ['bottom-border']
            };

            $scope.autocompleteInput.itemSelectCallback = $scope.setModel;
            $scope.autocompleteInputAsync = {
                'propNameToDisplay': 'name',
                'isAsyncSearch': true,
                 'ID':'AsyncData',
                'placeholder': 'Search by Name  ',
                'listClass': ['list-border-energized'],
                'labelContainerClass': ['bottom-border']
            };
            $scope.autocompleteInputAsync.itemSelectCallback = $scope.setModelAsync;
            $scope.autocompleteInputAsync.asyncHttpCall = $scope.asyncSearch;
            

               

        }]);

;
