// Ionic Geofence example App

angular.module('beerPass', ['ngCordova','ion-place-tools','ionic-autocomplete','ionic', 'angularGeoFire','firebase', 'pdf'])
    
    .config(['$compileProvider',
    function ($compileProvider) {

      $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|local|data|content|cdvfile):/);
    
    }])

    .config(['$httpProvider','$stateProvider', '$urlRouterProvider',function ($httpProvider,$stateProvider, $urlRouterProvider) {
     $stateProvider

            .state('app', {
              url: "/app",
              abstract: true,
              templateUrl: "views/menu.html",
              controller: 'MenuCtrl'
            })

             .state('app.intro', {
              url: "/intro",
              views: {
                'menuContent' :{
                  templateUrl: "views/intro.html",
                  controller: 'introCtrl'
                }
              }
            })

            .state('app.locations', {
                url: '/locations',
                views: {
                  'menuContent' :{
                    templateUrl: 'views/locations.html',
                    controller: 'DemoCtrl'
                  }
              }
                
            })

            .state('app.claims', {
                //url: '/claims/:var',
                url: '/claims/',
                //params:{'var':null},
                views: {
                  'menuContent' :{
                    templateUrl: 'views/claims.html',
                    controller: 'ClaimCtrl'
                  }
              }
                
            })

            .state('app.notary', {
                url: '/notary',
                views: {
                  'menuContent' :{
                    templateUrl: 'views/notary.html',
                    controller: 'DemoCtrl'
                  }
              }
                
            })

            .state('app.mapNotary', {
                url: '/mapNotary',
                views: {
                  'menuContent': {
                    templateUrl: 'views/breweries.html',
                    controller: 'MyCtrl'
                  }
                }
              })

            .state('app.rate', {
                url: '/rate',
                views: {
                  'menuContent': {
                    templateUrl: 'views/rate.html',
                    controller: 'introCtrl'
                  }
                }
              })


        $urlRouterProvider.otherwise('app/intro');

    }])
    .run(function ($rootScope, $window, $document, $ionicLoading, $state, $ionicPlatform, $log, $rootScope) {
        $ionicPlatform.ready(function () {
          var app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
            if ( app ) {
              console.log('this is an app');
              $rootScope.appType=1;
                // PhoneGap application
            } else {
                // Web page
                console.log('this is an web page');
                $rootScope.appType=2;
            }
            
            $log.log('Ionic ready');
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if ($window.cordova && $window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if ($window.StatusBar) {
                $window.StatusBar.styleDefault();
            }

    })

    })



