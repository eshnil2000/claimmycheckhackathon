(function () {
    angular.module('beerPass').factory('InvoiceService', ['$q', InvoiceService]);

    function InvoiceService($q,$rootScope) {
        function createPdf(invoice) {
            return $q(function (resolve, reject) {
                var dd = createDocumentDefinition(invoice);
                var pdf = pdfMake.createPdf(dd);

                pdf.getBase64(function (output) {
                    resolve(base64ToUint8Array(output));
                });
            });
        }

        return {
            createPdf: createPdf
        };
    }
    function createDocumentDefinition(invoice,$rootScope) {
        console.log(invoice.ClaimInfo.claimOrDonate);
        claimType=invoice.ClaimInfo.claimType;
        if (claimType==1){
            typeCLaim='an Individual';
        }
        if (claimType==2){
            typeCLaim='a Business';
        }
        if (claimType==3){
            typeCLaim='a Joint Claim';
        }
        claimOrDonate=invoice.ClaimInfo.claimOrDonate;
        if (claimOrDonate==1){
            claimOrDonate='';
        }
        if (claimOrDonate==2){
            claimOrDonate='I agree to Donate the whole amount to the County of Sacramento';
        }

        
    
        var dd = {
            content: [
                { text: 'COUNTY OF SACRAMENTO UNCLAIMED WARRANT AFFIDAVIT FOR  '+typeCLaim, alignment: 'center',style: 'header' },
                { text: invoice.Date, alignment: 'center' },
                { text: 'I, '+invoice.ClaimInfo.Name+ ' ,do hereby state that I am legal owner or custodian of Sacramento County Warrant No:' +
                    invoice.ClaimInfo.CheckNo+
                    ' dated: ' +
                    invoice.ClaimInfo.Dated +
                    ' in the amount of $'+
                    invoice.ClaimInfo.Amount +
                    '. '    
                },
                { text: claimOrDonate},
                { text: 'I declare under penalty of perjury that the above information is true and correct to the best of my knowledge and was executed on  ' +
                      invoice.Date+  
                      ' at Sacramento, California.'
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'
                },

                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'
                    },

                { text: '____________________'
                    },
                { text: 'Payee Signature'   
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'
                },
                { text: invoice.ClaimInfo.Telephone 
                    },
                { text: '__________________________'
                    },
                { text: 'Payee Telephone' 
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'
                },
                { text: invoice.ClaimInfo.Address 
                    },
                { text: '_________________________'
                    },
                { text: 'Payee Address'
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'
                },
                { text: '____________________________________________________________'
                    },
                 { text: 'Payee City, State and Zip code'
                    },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'
                },
                { text: 'State of _______________________)'
                },
                { text: '________________________________)SS'
                },
                { text: 'County of ______________________)'
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t Subscribed and sworn to (or affirmed) before me on this'
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t____ \t\t day of ____________,______, by'
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t Date \t\t\t\t\t\t\t\t\t\t\t\t\t\t Month,\t\t\t\t\t\t\t\t\t\t\tYear'
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ___________________________________________________'
                    },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t Name of Signer'
                },  
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t proved to me on the basis of satisfactory evidence to be'
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t the person who appeared before me.'
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'
                },
                { text: 'Affix Notary Seal Above\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ________________________________________'
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\ Signature of Notary Public'
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'
                },
                { text: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'
                },
                { text: 'THIS AFFIDAVIT MUST BE NOTARIZED IF THE CLAIM AMOUNT IS GREATER THAN $100.',alignment: 'center'
                },
                { text: 'SEND COMPLETED FORM TO: COUNTY OF SACRAMENTO, AUDITOR-CONTROLLER, 700 H ST, ROOM 3650, SACRAMENTO, CA 95814',alignment: 'center'
                }

            ],
            styles: {
                header: {
                    fontSize: 20,
                    bold: true,
                    margin: [0, 0, 0, 10],
                    alignment: 'right'
                },
                subheader: {
                    fontSize: 16,
                    bold: true,
                    margin: [0, 20, 0, 5]
                },
                itemsTable: {
                    margin: [0, 5, 0, 15]
                },
                itemsTableHeader: {
                    bold: true,
                    fontSize: 13,
                    color: 'black'
                },
                totalsTable: {
                    bold: true,
                    margin: [0, 30, 0, 0]
                }
            },
            defaultStyle: {
            }
        }

        return dd;
    }

    function createDocumentDefinitionBackUp(invoice) {

        var items = invoice.Items.map(function (item) {
            return [item.Description, item.Quantity, item.Price];
        });

        var dd = {
            content: [
                { text: 'INVOICE', style: 'header' },
                { text: invoice.Date, alignment: 'right' },

                { text: 'From', style: 'subheader' },
                invoice.AddressFrom.Name,
                invoice.AddressFrom.Address,
                invoice.AddressFrom.Country,

                { text: 'To', style: 'subheader' },
                invoice.AddressTo.Name,
                invoice.AddressTo.Address,
                invoice.AddressTo.Country,

                { text: 'Items', style: 'subheader' },
                {
                    style: 'itemsTable',
                    table: {
                        widths: ['*', 75, 75],
                        body: [
                            [
                                { text: 'Description', style: 'itemsTableHeader' },
                                { text: 'Quantity', style: 'itemsTableHeader' },
                                { text: 'Price', style: 'itemsTableHeader' },
                            ]
                        ].concat(items)
                    }
                },
                {
                    style: 'totalsTable',
                    table: {
                        widths: ['*', 75, 75],
                        body: [
                            [
                                '',
                                'Subtotal',
                                invoice.Subtotal,
                            ],
                            [
                                '',
                                'Shipping',
                                invoice.Shipping,
                            ],
                            [
                                '',
                                'Total',
                                invoice.Total,
                            ]
                        ]
                    },
                    layout: 'noBorders'
                },
            ],
            styles: {
                header: {
                    fontSize: 20,
                    bold: true,
                    margin: [0, 0, 0, 10],
                    alignment: 'right'
                },
                subheader: {
                    fontSize: 16,
                    bold: true,
                    margin: [0, 20, 0, 5]
                },
                itemsTable: {
                    margin: [0, 5, 0, 15]
                },
                itemsTableHeader: {
                    bold: true,
                    fontSize: 13,
                    color: 'black'
                },
                totalsTable: {
                    bold: true,
                    margin: [0, 30, 0, 0]
                }
            },
            defaultStyle: {
            }
        }

        return dd;
    }

    function base64ToUint8Array(base64) {
        var raw = atob(base64);
        var uint8Array = new Uint8Array(raw.length);
        for (var i = 0; i < raw.length; i++) {
            uint8Array[i] = raw.charCodeAt(i);
        }
        return uint8Array;
    }
})();