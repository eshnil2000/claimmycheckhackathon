This repository includes the two files that **must be included** in all project submissions for Sacramento County's [Hack4Sac Civic Tech Challenge](http://hack4sac.saccounty.net/):

1. this `README` containing instructions and information to include in your submission; and,
2. a `LICENSE` that allows anyone to do anything they want with your code as long as they provide attribution back to you and don’t hold you liable.

## Project Information
ClaimMyCheck makes it a breeze to search for, share and claim Unclaimed Checks through a combination of web app, Android + IOS App & an easy to use RESTful API. 

*Please complete the information below:*

### Project Name
Claim my Check
###On the App Store
![alt tag](https://bitbucket.org/eshnil2000/claimmycheckhackathon/raw/master/images/ClaimMyCheckAppStore.png)

### Project Description
[Make it a breeze to locate & claim unclaimed checks]

### Team Members
- [Team Leader: Nilesh Shah, hackfor@gmail.com]
- [Additional Team Members, email address]
- [...]

this README containing instructions and information to include in your submission; and,
a LICENSE that allows anyone to do anything they want with your code as long as they provide attribution back to you and don’t hold you liable.
Project Name: SacCountyConnect

###Use case with App

###Lookup by name, check number

![alt tag](https://bitbucket.org/eshnil2000/claimmycheckhackathon/raw/master/images/start1.gif)


### SMall amount Email claim

![alt tag](https://bitbucket.org/eshnil2000/claimmycheckhackathon/raw/master/images/nonotary2.gif)


###Large amount, quick find a notary

![alt tag](https://bitbucket.org/eshnil2000/claimmycheckhackathon/raw/master/images/notary3.gif)


###Notary finder on map

![alt tag](https://bitbucket.org/eshnil2000/claimmycheckhackathon/raw/master/images/notarymap5.gif)


###App on Android App Store
https://play.google.com/store/apps/details?id=com.eshnil2000.ClaimMyCheck

###Hosted Web App
http://54.245.107.178:8100

###App on IOS App Store
pending approval

###What are we trying to solve?

Currently it is difficult for the County of Sacramento to reconcile Unclaimed checks that have been issued and mailed but remain uncashed 6 months after the issue date. These checks are Listed in searchable pdf on website and on open data portal but this is not easy to use.
Department of Finance has a procedure to claim; laws govern process
–Total unclaimed amount listed now: $1.89M. But the process is not user friendly. 

###Enter ClaimMyCheck 
ClaimMyCheck makes it a breeze to search for, share and claim Unclaimed Checks through a combination of web app, Android + IOS App & an easy to use RESTful API. 

Look up your checks by Name, Check Number.

Share with others on social media if you found a friends check.

The App guides you through the Claim filing process, allows you to fill the right forms in the App itself and then mail it to the right contact at the County. 

The App also allows you to locate Certified Notaries all across California through an easy to use Map interface or a quick lookup by Zip Code. 

Team Members

[Samaira Shah, sssportstar@gmail.com]
[Nilesh Shah, hackforsac@gmail.com]


### Screen shots
![alt tag](https://bitbucket.org/eshnil2000/claimmycheckhackathon/raw/master/images/search.gif)

![alt tag](https://bitbucket.org/eshnil2000/claimmycheckhackathon/raw/master/images/claim1.gif)

![alt tag](https://bitbucket.org/eshnil2000/claimmycheckhackathon/raw/master/images/submit.gif)

![alt tag](https://bitbucket.org/eshnil2000/claimmycheckhackathon/raw/master/images/notary.gif)

![alt tag](https://bitbucket.org/eshnil2000/claimmycheckhackathon/raw/master/images/send.gif)

Stakeholder Engagement

Thanks to David Villanueva from Sacramento County for pitching the problem statement and providing a challenging opportunity to hack unorganized County data! Thanks also to Jose Ponce from the City of Sacramento for confirming the APIs are immediately usable, and the City was thinking of implementing such a web site. Also thanks to him for confirming the extreme simplification of the entire process to claim unclaimed checks.
Screen shots


Developer Documentation

The app is designed with mobile in mind, is platform agnostic and extremely versatile and efficient: a single code base can be used as a web app, mobile app and compiled for an Android device app and an ios app without chaning a single line of code. This is a hybrid app. The data is served through a RESTful interface via a node server. The data can also be served via a database as a service firebase, and has been converted into JSON format. The raw data from the County was made available as flat files, pdfs, etc. We converted the data into an app friendly format (json) by going through 2 stages of transformation: 1. normalized the raw data into a csv format. 2. developed a script to convert the normalized data into json format. 3. converted the data into a realtime, web socket enabled database (Firebase) which facilitates real time search, lookup and geoquery features without requiring a single line of server code for the County!

Additional Information

The data can be easily modified through the provided APIs.

Instructions

To prepare the app: 1. git clone the repo 2. npm install 3. bower install. This will install all the required plugins for building the mobile app.

To see the hosted version demo, go to: http://54.245.107.178:8100

To compile as android app: ionic build -debug android this will output the apk file under the platforms/ folder

To view locally: in the main directory ionic serve

To compile for ios app: ionic build -debug ios this will output the installable file under the platforms/ folder

The firebase database is available at:  https://unclaimed3.firebaseio.com

To facilitate a server-less design, we've ported the data over to firebase. No need to run your own server for mobile apps! 

The app not only allows searching for unclaimed checks by name, check number, but allows sharing on social channels. 
The app guides the user through the complex process and selects the correct forms based on several checks on the Value of the check. The app allows the user to edit the form & submit the form by converting it into pdf format for easy submission. 
There is also the facility to search for notaries if the Amount exceeds the prescribed limit ($100 at present) by the County.
FInally, the app provides the ability to directly mail the form after it's been filled out, all through a simple click button process. 

The data is indexed on all variables & also can search partial string matches to facilitate autocomplete features & realtime responsiveness. Firebase can be easily scaled to support millions of users & millions of data points. At present, Sacramento County AND City of Sacramento Unclaimed Check data has been uploaded. FOllowing the same format, Unclaimed Check data from virtually every county in California could be uploaded to this database. It's hosted on Google Firebase, realtime scalable, Mongodb database with web sockets. This can be indexed on Check number, Payee Name, Check Date. Example API (can copy and paste into browser) to query by Name: https://unclaimed3.firebaseio.com/saccounty.json?orderBy="Payee_Name"&startAt="A"&limitToFirst=5. To all : Please be respectful & don't abuse the API, else I will have to turn on authentication & limit access.
 
other examples, query by date:  https://unclaimed3.firebaseio.com/saccounty.json?orderBy="Check_Date"&startAt="02/23/16"&limitToFirst=5
query by amount: https://unclaimed3.firebaseio.com/saccounty.json?orderBy="Check_Amount"&startAt="$60"&limitToFirst=5

query by check number: https://unclaimed3.firebaseio.com/saccounty.json?orderBy="Check_Number"&startAt=8000000000&limitToFirst=5
Example return value to query by Name: {"A1TOWING":{"Check_Amount":"$68.34","Check_Date":"8/5/13","Check_Number":"1101690472","Payee_Name":"A1 TOWING"},"AAMIRALMUSAWWIR":{"Check_Amount":"$29.44","Check_Date":"5/28/15","Check_Number":"1102031099","Payee_Name":"AAMIR ALMUSAWWIR"},"AARONBETINIS":{"Check_Amount":"$412.02","Check_Date":"1/5/15","Check_Number":"1101956921","Payee_Name":"AARON BETINIS"},"ACMOK":{"Check_Amount":"$26.32","Check_Date":"5/13/15","Check_Number":"1102022503","Payee_Name":"A C MOK"},"ACMok":{"Check_Amount":"$22.51","Check_Date":"4/15/15","Check_Number":"1102006349","Payee_Name":"A C Mok"}}

In fact, we've indexed all the notaries in california by zip code, and created an API: curl -X GET https://notaryca.firebaseio.com/list.json?orderBy="Zip_Code"&startAt="95630"&endAt="95630"&limitToFirst=5

To print out in a nice, readable format, add print=pretty at the end: https://unclaimed3.firebaseio.com/saccounty.json?orderBy=%22Check_Amount%22&startAt=%22$60%22&limitToFirst=5&print=pretty

FUTURE ENHANCEMENTS: 
