//Node.js script to convert http://data.saccounty.net/home data to 
//web app/ mobile app friendly data
//Unclaimed check data is at: http://data.saccounty.net/dataviews/226166/outstanding-checks-from-county-of-sacramento/
//Step 0: at command line, run npm install fast-csv
//Step 1: Export to csv, save locally as "OutstandingChecks.csv"
//Step 2: Run this script, convert data to JSON object
//Step 3: Upload data to your favorite web friendly database
//some required node modules
//Parse csv files
var csv = require("fast-csv");

//call the function
convertCsv2JSON();

//the actual function
function convertCsv2JSON () {
	var nameArray=[];
	var namePath=null;
	csv
	 .fromPath("OutstandingChecks.csv")
	 .on("data", function(item){
	 	var namePath=item[3].trim().replace(/[^a-zA-Z0-9]/g, "");
	 	var obj={
		 	'Check_Date':item[0],
		 	'Check_Number':item[1],
		 	'Check_Amount':item[2],
		 	'Payee_Name':item[3]
		 };
	 		nameArray[namePath]=obj;
	 })
	 .on("end", function(){
	     console.log("done");
	     console.log(nameArray);
	 	 //upload JSON array to your favorite database
	 });

	}
